#!/usr/bin/bash
python3 -m venv venv
. ./venv/bin/activate
pip install wheel
pip install --upgrade pip
pip install -r requirements.txt
echo "run '. ./venv/bin/activate' to activate the virtual environment"
echo "run 'deactivate' to deactivate the virtual environment"
