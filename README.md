# SAN Tools Project

Just a place to put  examples for querying SAN switches and arrays to
collect data for reporting, alerting and trending.

Clone the repository
cd into the project directory
run the setup.py shell script to create a virtual environment

If you run the setup script in the current shell, you will have
activated the virtual environment (venv) as indicated by the prompt:

`
slay@pure08v99ev52:~/python_projects/san-tools$ . ./setup.bash
Collecting wheel
 . . .
run '. ./venv/bin/activate' to activate the virtual environment
run 'deactivate' to deactivate the virtual environment
(venv) slay@pure08v99ev52:~/python_projects/san-tools$. ./setup.bash
`

The utilities are described below:

_**scan_switches.py**_:

Usage: python3 scan_switches.py <yaml file of IPs>

This tool gathers CMDB updata for SAN switches.
It reads IP addresses for Cisco and Brocade switches
from the YAML file on the command line.
The format of the file is:

`
---
cisco_ips:
  - 0.0.0.0

brocade_ips:
  - 0.0.0.0
`

Non-responsive IPs will time-out and report a record containg
only the IP address and the manufacturer.
