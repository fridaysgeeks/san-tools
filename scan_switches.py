import sys
from getpass import getpass as getpass
import re
import paramiko
from pprint import pprint
import os
import yaml

brocade_model_map = { '165': 'X6-4',
                      '166': 'X6-8',
                      '162': 'G620',
                      '183': 'G620',
                      '170': 'G610',
                      '173': 'G630',
                      '184': 'G630',
                      '178': '7810',
                      '179': 'X7-4',
                      '180': 'X7-8',
                      '181': 'G720',
                      '189': 'G730' }
   

def read_yaml(file_path):
    with open(file_path, 'r') as file:
        try:
            yaml_content = yaml.safe_load(file)
        except Exception as err:
            print(err)
    return yaml_content

def cisco_get_cmdb_data(hostname, username, password, sshport=22):
    name = wwn  = model_id = serial_number = location = ''
    san = firmware = model_number = desc_line_next = description = ''
    manufacturer = 'Cisco'
    
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(hostname, sshport, username, password, timeout=5.0)

        cmd = 'show hardware'
        stdin, stdout, stderr = ssh.exec_command(cmd)
        stdin.close()
        stderr.close()
        for line in stdout.readlines():
            line = line.strip()
            fields = line.split()
            if line == '':
                continue
            if fields[0] == 'system:' and firmware == '':
                firmware = fields[-1]
            elif fields[0] == 'Hardware' and description == '':
                desc_line_next = True
            elif desc_line_next == True:
                description = line
                desc_line_next = ''
            elif re.match('^Model number is', line) and model_number == '':
                model_number = fields[-1]
            elif re.match('^Serial number is', line) and serial_number == '':
                serial_number = fields[-1]
            elif re.match('^Device name:', line) and name == '':
                name = fields[-1]

        cmd = 'show wwn switch'
        stdin, stdout, stderr = ssh.exec_command(cmd)
        stdin.close()
        stderr.close()
        for line in stdout.readlines():
            line = line.strip()
            fields = line.split()
            if re.match('^Switch WWN.*', line):
                wwn = fields[-1]
        stdout.close()
        ssh.close()
        
    except Exception as err:
        e = err
    finally:
        return(dict(name=name, wwn=wwn, manufacturer=manufacturer, \
                    san=san,   ip=ip,   model_number=model_number, \
                    location=location,  description=description,   \
                    firmware=firmware,  serial_number=serial_number))
                                             

def brocade_get_cmdb_data(hostname, username, password, sshport=22):
    name = wwn  = model_id = serial_number = location = ''
    san = firmware = model_number = desc_line_next = description = ''
    manufacturer = 'Brocade'
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(hostname, sshport, username, password, timeout=5.0)
        """ Run the 'switchshow' command to get name and model ID """
        cmd = 'switchshow'
        stdin, stdout, stderr = ssh.exec_command(cmd)
        stdin.close()
        stderr.close()
        for line in stdout.readlines():
            line = line.strip()
            fields = line.split()
            if fields[0] == 'switchName:':
                name = fields[1]
            elif fields[0] == 'switchType:':
                switch_type = fields[1][0:3]
                model_number = brocade_model_map[switch_type]
            else:
                continue
        stdout.close
        
        """ run the 'firmwareshow' command to get the firware version"""
        cmd = 'firmwareshow'
        stdin, stdout, stderr = ssh.exec_command(cmd)
        stdin.close()
        stderr.close()
        for line in stdout.readlines():
            line = line.strip()
            if re.match('^FOS.*', line):
                tag, firmware = line.split()
        stdout.close
        
        """ Run 'wwn -sn' command to get the wwnn and serial number of the switch"""
        cmd = 'wwn -sn'
        stdin, stdout, stderr = ssh.exec_command(cmd)
        stdin.close()
        stderr.close()
        for line in stdout.readlines():
            if re.match('^WWN: ', line):
                wwn = line.split()[1]
            elif re.match('^SN: ', line):
                serial_number = line.split()[1]
        stdout.close
        ssh.close()
    except Exception as err:
        e = err
    finally:
        return(dict(name=name, wwn=wwn, manufacturer=manufacturer, \
                    san=san,   ip=ip,   model_number=model_number, \
                    location=location,  description=description,   \
                    firmware=firmware,  serial_number=serial_number))

def print_cmdb_header():
    print('"Name","WWNN","Manufacturer","Model ID",'  + \
          '"Description","Serial number","Location",' + \
          '"SAN","ip_address","firmware"')
    
def print_cmdb_data(data_dict):
    print(('"{}",' * 9 + '"{}"').format(data_dict['name'],          \
                                        data_dict['wwn'],           \
                                        data_dict['manufacturer'],  \
                                        data_dict['model_number'],  \
                                        data_dict['description'],   \
                                        data_dict['serial_number'], \
                                        data_dict['location'],      \
                                        data_dict['san'],           \
                                        data_dict['ip'],            \
                                        data_dict['firmware']))

if __name__ == "__main__":
    print(sys.argv)
    if len(sys.argv) != 2:
        print('Usage: {} <yaml file of IPs>'.format(sys. argv[0]))
    
    username = input('User name:  ')
    password = getpass('Password: ')

    ip_lists = read_yaml(sys.argv[1])

    print_cmdb_header()

    for ip in ip_lists['cisco_ips']:
        print_cmdb_data(cisco_get_cmdb_data(ip, username, password))

    for ip in ip_lists['brocade_ips']:
        print_cmdb_data(brocade_get_cmdb_data(ip, username, password))
